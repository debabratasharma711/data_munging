""" 
The method in this module reads from config.ini from the corresponding field of the section
"""

from configparser import ConfigParser

def config_reader(section_name, field_name):
        """
            input arg1: name of section 
            input arg2: name of field
            output: value of the field in that section
            output type: string
        """
        
        reader = ConfigParser()
        reader.read('config.ini')
        return reader[section_name][field_name]
