""" This module assigns instance variables of to object of the defined class and invokes methods of
inherited class to extract and analyse data"""
from data_analysis import DataAnalyser
#from data_analysis import DataManager
from config_parser import config_reader


class FootballDataAnalyser(DataAnalyser):
    """ 
    This is the child class of DataAnalyser
    """

    def __init__(self, section_name, file_name, file_path, max_value_index, min_value_index, day_index):
            """ 
            instances in constructor are assigned corresponding values invoked by config_reader 
            method in config_parser module 
            """
            
            self.file_name = config_reader(section_name, file_name)
            self.file_path = config_reader(section_name, file_path)
            self.max_value_index = int(config_reader(section_name, max_value_index))
            self.min_value_index = int(config_reader(section_name, min_value_index))
            self.day_index = int(config_reader(section_name, day_index))


    def display_team_name(self, team_name, goal_diff):
            """ 
            displays team name with minimum difference between for and against goal
            """
            print(team_name + " has the minimum difference of " + str(int(goal_diff)) + \
                " between for and against goals among all the teams")



football_obj = FootballDataAnalyser('file_extract_param', 'football_file_name', 'football_file_path',\
                        'football_for_goal_index', 'football_against_goal_index', 'team_name_index')
list_football_data = football_obj.extract_data(football_obj.file_path)
clean_data = football_obj.clean_data(list_football_data, football_obj.max_value_index,\
                                    football_obj.min_value_index)
team_name, diff_goals = football_obj.cal_diff(clean_data, football_obj.day_index, \
                        football_obj.max_value_index, football_obj.min_value_index)
football_obj.display_team_name(team_name, diff_goals)