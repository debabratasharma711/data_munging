""" This module assigns instance variables of to object of the defined class and invokes methods of
inherited class to extract and analyse data"""
from data_analysis import DataAnalyser
from config_parser import config_reader


class WeatherDataAnalyser(DataAnalyser):
    """ 
    This is the child class of DataExtractor and TransformData classes
    """

    def __init__(self, section_name, file_name, file_path, max_value_index, min_value_index, day_index):
            """ 
            instances in constructor are assigned corresponding values invoked by config_reader 
            method in config_parser module 
            """

            self.file_name = config_reader(section_name, file_name)
            self.file_path = config_reader(section_name, file_path)
            self.max_value_index = int(config_reader(section_name, max_value_index))
            self.min_value_index = int(config_reader(section_name, min_value_index))
            self.day_index = int(config_reader(section_name, day_index))


    def display_day_temp_diff(self, day_no, temp_diff):
            """
            displays the day number with minimum temperature difference 
            """
            
            print("Day " + day_no + " has the minimum temperature difference of " + str(int(temp_diff)) + \
                " between max and min temperature")


weather_obj = WeatherDataAnalyser('file_extract_param', 'weather_file_name', 'weather_file_path', 'weather_max_temp_index',\
                      'weather_min_temp_index', 'day_index')
list_weather_data = weather_obj.extract_data(weather_obj.file_path)
clean_data = weather_obj.clean_data(list_weather_data, weather_obj.max_value_index, weather_obj.min_value_index)
day, temp_diff = weather_obj.cal_diff(clean_data, weather_obj.day_index, weather_obj.max_value_index,\
                                    weather_obj.min_value_index)
weather_obj.display_day_temp_diff(day, temp_diff)