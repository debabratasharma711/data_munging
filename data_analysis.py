""" 
This module does all the data operations
"""

import sys
from re import findall
class DataManager:
    """ 
    This is the parent class
    """
    def extract_data(self, file_path):
        """ 
        input arg: file path
        input type: string
        output: list of data in file
        output type: list 
        """
        extract_data = open(file_path, "r")
        list_each_row = []
        list_extracted_data = []
        row_count = 0 # flag to exclude 1st row of the file
        for each_row in extract_data:
            if row_count == 0 :
                row_count += 1
                continue
            list_each_row = each_row.split()
            list_extracted_data.append(list_each_row)
        return list_extracted_data

        """ 
        This is the parent class
        """

    def clean_data(self, extracted_data_list, max_value_index, min_value_index):
        """ 
        input:
            arg1: list data | type: list
            arg2: index of attribute with max value
            arg3: index of attribute with min value
        output:
            cleaned data
            type: list
        """

        clean_data = []
        for each_row in extracted_data_list:
            if len(each_row) < min(max_value_index, min_value_index):
            #data_not_available
                continue
            else:
                max_val_temp = findall(r"[-+]?\d*\.\d+|\d+",each_row[max_value_index])[0]
                min_val_temp = findall(r"[-+]?\d*\.\d+|\d+",each_row[min_value_index])[0]
                if max_val_temp.isdigit()==True or max_val_temp.replace(".", '',1).isdigit == True or\
                    min_val_temp.isdigit()==True or min_val_temp.replace(".", '',1).isdigit == True:

                        each_row[max_value_index] = max_val_temp
                        each_row[min_value_index] = min_val_temp
                        clean_data.append(each_row)

        return clean_data

class DataAnalyser(DataManager):   

    def cal_diff(self, clean_data_list, day_or_team_name_index, max_value_index, min_value_index):
        """ 
        input
            arg1: list data  | type: list
            arg2: index of day number or team name depending on the calling object  |  type: str
            arg3: index number of max temperature or for goals depending on the calling object | type : str
            arg4: index number of min temperature or against goals depending on the calling object | type : str
        output
            arg1: day or team name depending on the calling object | type : str
            arg2: minimum temperatur difference or goal difference depending on calling object | type: str
        """
            
        min_diff = sys.maxsize
        for each_row in clean_data_list:
            if (abs(float(each_row[max_value_index]) - float(each_row[min_value_index]))) <= min_diff :
                day_or_team_name = each_row[day_or_team_name_index]
                min_diff = abs(float(each_row[max_value_index]) - float(each_row[min_value_index]))

        return day_or_team_name, min_diff