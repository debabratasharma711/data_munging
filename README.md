# DATA MUNGING PROJECT

This project is based on implementation of OOPs concepts, precisely the SOLID concept. \
This project has a couple of tasks:\
    a. Output the day number (column one) with the smallest temperature spread.\
    b. Print the name of the team with the smallest difference in ‘for’ and ‘against’ goals.\
\
The directory structure is as shown:

    .
    ├── common.py
    ├── config.ini
    ├── config_parser.py
    ├── weather_analysis.py
    ├── data
    │   ├── football.dat
    │   └── weather.dat
    └── football_analysis.py
        

### Data files description:
    a. football.dat : daily weather data for Morristown, NJ for June 2002.
    b. weather.dat : results from the English Premier League for 2001/2. The columns labeled ‘F’ and ‘A’ contain the total number of goals scored for and against each team in that season (so Arsenal scored 79 goals against opponents, and had 36 goals scored against them)

config.ini contains all the hard coded values used in the project.\
Working of each module is mentioned in the doc string.\
Run the weather_analysis.py file to for the first task and football_analyser.py file to run for the second. 


